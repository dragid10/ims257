//add document ready here
$(document).ready(function () {
    $("#first_link").focus();
    $("#image_list").find("a").each(function () {
        var link = $(this).attr("href");
        var caption = $(this).attr("title");
        $(this).click(function (evt) {
            evt.preventDefault();
            $("#image").attr("src", link);
            $("#caption").val(caption);
        })
    })
});