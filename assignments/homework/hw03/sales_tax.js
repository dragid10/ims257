var $ = function (id) {
    return document.getElementById(id);
}
var calculate_click = function () {
    var subtotal = parseFloat($("subtotal").value);
    var taxRate, salesTax = 0, total = 0;
    // debugger;
    if (subtotal > 0 && subtotal < 30000) {
        taxRate = parseFloat($("tax_rate").value);
        if (taxRate > 0 && taxRate < 15) {
            notValid = true
            salesTax = subtotal * taxRate / 100;
            total = subtotal + salesTax;
            $("sales_tax").value = salesTax;
            $("total").value = total;
        } else {
            alert("Must be a positive number less than 15");
        }
    } else {
        alert("Must be a positive number less than $30,000");
    }


}
var clear_click = function () {
    document.getElementById("subtotal").value = "";
    document.getElementById("tax_rate").value = "";
    document.getElementById("sales_tax").value = "";
    document.getElementById("total").value = "";
    document.getElementById("subtotal").focus();
}

window.onload = function () {
    document.getElementById("calculate").addEventListener("click", calculate_click);
    document.getElementById("clear").addEventListener("click", clear_click);
}