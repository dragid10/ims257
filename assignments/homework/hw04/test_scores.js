var scoreList = [];
var textDisplay = "";

var $ = function (id) {
	return document.getElementById(id);
};

function addElement() {
	var studentName = document.getElementById("name").value;
	var studentScore = document.getElementById("score").value;

	if (studentName !== "" && studentScore >= 0 && studentScore <= 100) {
		scoreList.push({ 'name': studentName, 'score': studentScore });
		document.getElementById("name").value = "";
		document.getElementById("score").value = "";

		document.getElementById("name").focus();
	} else {
		alert("Must enter a name and/or a valid score");
	}

}

var listArray = function () {
	scoreList.sort(numSort);
	console.log(scoreList);
	var highestScore = scoreList[scoreList.length - 1].score, lowestScore = scoreList[0].score, averageScore = getAverage();

	textDisplay = "Average Score = " + averageScore + "\nHighest Score = " + highestScore + "\nLowest Score = " + lowestScore + "\n\n";

	for (var i = 0; i < scoreList.length; i++) {
		textDisplay += scoreList[i].name + ", " + scoreList[i].score + "\n";
	}
	document.getElementById("results").value = textDisplay;
	document.getElementById("name").focus();
};

function numSort(x, y) {
	return x.score - y.score;
}

var getAverage = function () {
	var total = 0, count = 0;
	for (var i = 0; i < scoreList.length; i++) {
		total += parseInt(scoreList[i].score);
		count++;
	}
	return (total / count);
};

window.onload = function () {
	document.getElementById("add").onclick = addElement;
	document.getElementById("display").onclick = listArray;
};


