var calculateMPG = function () {
	var miles_driven = document.getElementById("miles_driven").value;
	var gas_used = document.getElementById("gas_used").value;
    var MPG=parseFloat(miles_driven)/parseFloat(gas_used);
    document.getElementById("mpg").value = MPG;
}
window.onload = function () {
    document.getElementById("calculate").onclick = calculateMPG;
	document.getElementById("miles_driven").focus();
}
